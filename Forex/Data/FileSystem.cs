﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Forex.Models;


namespace Forex.Data
{
    public static class FileSystem
    {
        public static string Path = "C:\\Data\\Forex\\";
        public static TimeFrameCollection GetTimeFrame(string FileOrSymbol, string TimeFrame)
        {

            var sr = new System.IO.StreamReader(Path + FileOrSymbol + " " + TimeFrame + ".json");
            return Newtonsoft.Json.JsonConvert.DeserializeObject<TimeFrameCollection>(sr.ReadToEnd());
        }
        public static TimeFrameCollection GetTimeFrame(Guid CurrencyPairId, Enums.TimeFrame TimeFrame)
        {
            return GetTimeFrame(CurrencyPairId.ToString(), TimeFrame.ToString());
        }
        public static void SaveTimeFrame(TimeFrameCollection TimeFrame)
        {
            var File = !string.IsNullOrEmpty(TimeFrame.Symbol) ? TimeFrame.Symbol : TimeFrame.PairId.ToString();
            var sTimeFrame = !string.IsNullOrEmpty(TimeFrame.sTimeFrame) ? TimeFrame.sTimeFrame : TimeFrame.TimeFrame.ToString();
            var sw = new System.IO.StreamWriter(Path + File + " " + sTimeFrame + ".json");
            sw.Write(Newtonsoft.Json.JsonConvert.SerializeObject(TimeFrame));
            sw.Close();
        }
        public static void WriteList(string FileName, List<string> Strings)
        {
            if (System.IO.File.Exists(Path + FileName + ".txt"))
                System.IO.File.Delete(Path + FileName + ".txt");

            var sw = new System.IO.StreamWriter(Path + FileName + ".txt");
            Strings.ForEach(s => sw.WriteLine(s));
            sw.Close();
        }
        public static List<string> ReadList(string FileName)
        {
            var retList = new List<string>();

            if (!System.IO.File.Exists(Path + FileName + ".txt"))
                return retList;
            
            var sr = new System.IO.StreamReader(Path + FileName + ".txt");
            
            do
                retList.Add(sr.ReadLine());
            while (!sr.EndOfStream);
            
            return retList;

        }
    }
}
