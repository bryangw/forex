﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;

namespace Forex.Data.Configuration
{
    class CurrencyPair: EntityTypeConfiguration<Models.CurrencyPair>
    {
        public CurrencyPair()
        {
            Property(p => p.Pair).IsRequired();
            Property(p => p.pip).HasPrecision(6, 5);
            Property(p => p.cSpread).HasPrecision(2, 1);
            Property(p => p.pSpread).HasPrecision(2, 1);
            HasMany(p => p.Quotes).WithRequired(p => p.CurrencyPair);
            HasMany(p => p.Trades).WithRequired(p => p.CurrencyPair);
        }
    }
}
