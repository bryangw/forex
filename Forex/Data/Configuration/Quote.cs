﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
namespace Forex.Data.Configuration
{
    class Quote : EntityTypeConfiguration<Models.Quote>
    {
        public Quote()
        {
            Property(e => e.Open).HasPrecision(9, 5);
            Property(e => e.High).HasPrecision(9, 5);
            Property(e => e.Low).HasPrecision(9, 5);
            Property(e => e.Close).HasPrecision(9, 5);
            
        }

    }
}
