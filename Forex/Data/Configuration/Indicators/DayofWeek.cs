﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
namespace Forex.Data.Configuration.Indicators
{
    public class DayofWeek : EntityTypeConfiguration<Models.Indicators.DayofWeek>
    {
        public DayofWeek()
        {
            Property(e => e.Up).HasPrecision(5, 2);
            Property(e => e.Down).HasPrecision(5, 2);
        }
    }
}
