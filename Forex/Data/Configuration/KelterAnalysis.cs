﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;

namespace Forex.Data.Configuration
{
    public class KelterAnalysis : EntityTypeConfiguration<Models.KelterAnalysis>
    {
        public KelterAnalysis()
        {
            Property(p => p.OpenDistance).HasPrecision(9, 5);
            Property(p => p.LowDistance).HasPrecision(9, 5); 
            Property(p => p.HighDistance).HasPrecision(9, 5); 
            Property(p => p.CloseDistance).HasPrecision(9, 5);
            Property(p => p.High).HasPrecision(9, 5);
            Property(p => p.Low).HasPrecision(9, 5);
            Property(p => p.Open).HasPrecision(9, 5);
            Property(p => p.Close).HasPrecision(9, 5);
            Property(p => p.MovingAverage).HasPrecision(9, 5);
            Property(p => p.Bar1Close).HasPrecision(9, 5);
            Property(p => p.Bar2Close).HasPrecision(9, 5);
            Property(p => p.Bar3Close).HasPrecision(9, 5);
            Property(p => p.Bar4Close).HasPrecision(9, 5);
            Property(p => p.Bar5Close).HasPrecision(9, 5);
            Property(p => p.ATR).HasPrecision(9, 5);
        }
    }
}
