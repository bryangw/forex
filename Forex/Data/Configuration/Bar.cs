﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;

namespace Forex.Data.Configuration
{
    public class Bar : EntityTypeConfiguration<Models.Bar>
    {
        public Bar()
        {
            HasOptional(e => e.BarSet).WithMany(e => e.Bars).HasForeignKey(e=>e.BarSetId);
            Property(e => e.Open).HasColumnType("int");
            Property(e => e.Close).HasColumnType("int");
            Property(e => e.Range).HasColumnType("int");
            Property(e => e.Movement).HasColumnType("int");
            

                
        }

    }
}
