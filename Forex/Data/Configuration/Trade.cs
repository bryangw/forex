﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;

namespace Forex.Data.Configuration
{
    public class Trade : EntityTypeConfiguration<Models.Trade>
    {
        public Trade()
        {
            Property(e => e.OpenPrice).HasPrecision(9, 5);
            Property(e => e.ClosePrice).HasPrecision(9, 5);
            Property(e => e.StopLoss).HasPrecision(9, 5);
            Property(e => e.TakeProfit).HasPrecision(9, 5);
        }
    }
}
