﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;

namespace Forex.Data.Configuration
{
    public class BarTest : EntityTypeConfiguration<Models.BarTest>
    {
        public BarTest()
        {
            HasRequired(b => b.ActualBar).WithOptional().WillCascadeOnDelete(false);
            HasRequired(b => b.PredictedBar).WithOptional().WillCascadeOnDelete(false);
        }
    }
}
