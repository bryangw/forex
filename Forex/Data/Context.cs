namespace Forex.Data
{
    using System;
    using System.Data.Entity;
    using Conventions = System.Data.Entity.ModelConfiguration.Conventions;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Models;
    
    public partial class Context : DbContext
    {
        public DbSet<CurrencyPair> CurrencyPairs { get; set; }
        public DbSet<Quote> Quotes { get; set; }
        public DbSet<Models.Indicators.DayofWeek> Indicators_DayofWeek { get; set; }
        public DbSet<Models.Trade> Trades { get; set; }
        public Context()
            : base("name=Context")
        {
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<Conventions.ManyToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<Conventions.OneToOneConstraintIntroductionConvention>();
            modelBuilder.Conventions.Remove<Conventions.OneToManyCascadeDeleteConvention>();
            modelBuilder.Configurations.Add(new Configuration.Quote());
            modelBuilder.Configurations.Add(new Configuration.CurrencyPair());
            modelBuilder.Configurations.Add(new Configuration.Indicators.DayofWeek());
            modelBuilder.Configurations.Add(new Configuration.Trade());
        }
    }
}
