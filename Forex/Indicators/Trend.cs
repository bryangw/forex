﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forex.Indicators
{
    public class Trend : Indicator
    {
        private int LookBack { get; set; }
        public Trend(Forex.Business.Interval Interval, int LookBack) : base(Interval)
        {
            this.LookBack = LookBack;
        }
        public override int PushBar(Business.Bar Bar)
        {
            if (Interval.Bars.Count() < LookBack) { return 0; }
            var MovingAverage = Interval.Bars.OrderByDescending(b=>b.Time).Take(LookBack).Sum(b=>b.Close) / LookBack;
            
            if (Bar.Close == MovingAverage)
                return 0;
            
            return Bar.Close > MovingAverage ? 1 : -1;
        }
    }
}
