﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Forex.Business;

namespace Forex.Indicators
{
    public class DayOfWeek : Indicator
    {
        public static Dictionary<Guid, List<Models.Indicators.DayofWeek>> _DaysOfWeek { get; set; }
        public List<Models.Indicators.DayofWeek> DaysOfWeek
        {
            get
            {
                if (_DaysOfWeek.ContainsKey(Interval.CurrencyPair.Id))
                {
                    return _DaysOfWeek[Interval.CurrencyPair.Id];
                }
                var db = new Data.Context();
                var temp = db.Indicators_DayofWeek.Where(d => d.CurrencyPairId == Interval.CurrencyPair.Id).ToList();
                if (_DaysOfWeek == null)
                {
                    _DaysOfWeek = new Dictionary<Guid, List<Models.Indicators.DayofWeek>>();
                }
                _DaysOfWeek.Add(Interval.CurrencyPair.Id, temp);
                return _DaysOfWeek[Interval.CurrencyPair.Id];
            }
        }
        public DayOfWeek(Business.Interval Interval) : base(Interval) {
            if (_DaysOfWeek == null)
            {
                _DaysOfWeek = new Dictionary<Guid, List<Models.Indicators.DayofWeek>>();
            }
        }
        public override int PushBar(Bar Bar)
        {
            var DayofWeek = DaysOfWeek.Where(d => (int)d.DayOfWeek == (int)Bar.Time.DayOfWeek && d.CurrencyPairId == Interval.CurrencyPair.Id).FirstOrDefault();
            if (DayofWeek == null) { return 0; }
            if (DayofWeek.SafeBet < Business.Static.DayofWeek_SafeBet)
            {
                return 0;
            }
            return DayofWeek.Up > DayofWeek.Down ? 1 : -1;
        }
    }
}
