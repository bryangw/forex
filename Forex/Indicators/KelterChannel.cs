﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forex.Indicators
{
 /*   
public class KeltnerChannel : Indicator
{
   public List<KeltnerBar> Bars { get; set; }
   public List<Models.KelterAnalysis> Analysis { get; set; }
   public List<Models.KelterAnalysis> AnalysisCache { get; set; }
        
   public int Period { get; set; }
   public int ConsecutiveBars = 3;
   public double ATRPercentage = 0.05;


   public KeltnerChannel(int Period, int ConsecutiveBars = 3, double ATRPercentage = 0.05)
   {
       Bars = new List<KeltnerBar>();
       this.Period = Period;
       this.ConsecutiveBars = ConsecutiveBars;
       this.ATRPercentage = ATRPercentage;
       Analysis = new List<Models.KelterAnalysis>();
       AnalysisCache = new List<Models.KelterAnalysis>();
       db = Shared.GetDB();
   }
   public override int PushBar(Bar Bar)
   {
       var kBar = new KeltnerBar()
       {
           Open = Bar.Open,
           Low = Bar.Low,
           High = Bar.High,
           Close = Bar.Close,
       };
       Bars.Add(kBar);
       var count = Bars.Count();

       if (count < Period) { return 0; }
       if (count == Period)
       {
           kBar.MovingAverage = Shared.CalculateSMA(Bars);
           return 0;
       }
       var previousbar = Bars[count - 2];
       kBar.MovingAverage = Shared.CalculateEMA(previousbar, kBar, Period);
       kBar.ATR = Shared.CalculateATR(previousbar, kBar);
       if (Bars.Count() == Period + 1)
       {
           Bars.RemoveAt(0);
       }
       var KelterAnalysis = new Models.KelterAnalysis()
           {
               Close = kBar.Close,
               Open = kBar.Open,
               High = kBar.High,
               Low = kBar.Low,
               MovingAverage = decimal.Round(kBar.MovingAverage,5,MidpointRounding.ToEven),
               isUpBar = kBar.Open - kBar.Close > 0,
               CloseDistance = 0,
               HighDistance = 0,
               LowDistance = 0,
               OpenDistance = 0
           };
       if (kBar.ATR != 0)
       {
           KelterAnalysis.CloseDistance = decimal.Round((kBar.Close - kBar.MovingAverage) / kBar.ATR,5, MidpointRounding.ToEven);
           KelterAnalysis.OpenDistance = decimal.Round((kBar.Open - kBar.MovingAverage) / kBar.ATR, 5, MidpointRounding.ToEven);
           KelterAnalysis.HighDistance = decimal.Round((kBar.High - kBar.MovingAverage) / kBar.ATR,5,MidpointRounding.ToEven);
           KelterAnalysis.LowDistance = decimal.Round((kBar.Low - kBar.MovingAverage) / kBar.ATR,5,MidpointRounding.ToEven);
       }

       var UpperBound = Analysis.Count();
       if (UpperBound > 5) { UpperBound = 5; }
       for (var x = 0; x < UpperBound; x++)
       {
           switch (x)
           {
               case 0:
                   Analysis[0].Bar5Close = kBar.Close;
                   break;
               case 1:
                   Analysis[1].Bar4Close = kBar.Close;
                   Analysis[1].is1BarUp = Analysis[0].isUpBar;
                   break;
               case 2:
                   Analysis[2].Bar3Close = kBar.Close;
                   Analysis[2].is2BarUp = Analysis[0].isUpBar;
                   break;
               case 3:
                   Analysis[3].Bar2Close = kBar.Close;
                   Analysis[3].is3BarUp = Analysis[0].isUpBar;
                   break;
               case 4:
                   Analysis[4].Bar1Close = kBar.Close;
                   break;
           }
       }

       if (Analysis.Count == 5)
       {
           AnalysisCache.Add(Analysis[0]);
           //db.KelterAnalysis.Add(Analysis[0]);
           Analysis.RemoveAt(0);
           Records++;
           if (Records == 100)
           {
               try
               {
                   db.BulkInsert(AnalysisCache.ToArray());
               }
               catch (Exception ex) { }
               AnalysisCache = new List<Models.KelterAnalysis>();
                    
               Records = 0;
                    
           }
       }

       Analysis.Add(KelterAnalysis);
       return 0;
   }
    */

}
