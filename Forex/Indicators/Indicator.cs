﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Forex.Business;
using EntityFramework.BulkInsert.Extensions;
namespace Forex.Indicators
{
    
    public class Indicator
    {
        public int Records = 0;
        public Data.Context db;
        public List<Bar> Bars { get; set; }
        public Interval Interval { get; set; }
        public Models.Enums.IndicatorType Type { get; set; }
        public string IndicatorName { get; set; }
       
        public Indicator(Interval Interval)
        {
            this.Interval = Interval;
        }
        public virtual int PushBar(Bar Bar) { return 0; }
        public virtual decimal GetStopLoss() { return 0; }
   
    }
}
