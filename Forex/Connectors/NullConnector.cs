﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forex.Connectors
{
    public class NullConnector : ConnectorBase
    {
        public override TradeResult Execute(string Pair, decimal Size, Models.Enums.Position Position, DateTime Time = default(DateTime))
        {
            return new TradeResult() { Executed = false };
        }
    }
}
