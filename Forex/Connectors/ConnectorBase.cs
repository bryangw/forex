﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forex.Connectors
{
    public class TradeResult
    {
        public bool Executed { get; set; }
        public DateTime Time { get; set; }
        public decimal Price { get; set; }
    }
    public abstract class ConnectorBase
    {
        public abstract TradeResult Execute(string Pair, decimal Size, Models.Enums.Position Position, DateTime Time = default(DateTime));
    }

};
