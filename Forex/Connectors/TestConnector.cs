﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forex.Connectors
{
    public class TestConnector : ConnectorBase
    {
        public Models.CurrencyPair CurrencyPair { get; set; }
        public Models.TimeFrameCollection TimeFrame { get; set; }
        public TestConnector(string Pair, Models.Enums.TimeFrame TimeFrame)
        {
            this.CurrencyPair = Business.Static.CurrencyPairs.Where(c => c.Pair == Pair).First();
            this.TimeFrame = Data.FileSystem.GetTimeFrame(CurrencyPair.Id, TimeFrame);

        }
        public override TradeResult Execute(string Pair, decimal Size, Models.Enums.Position Position, DateTime Time)
        {
            var start = Time.AddMinutes(-Math.Ceiling((double)(int)TimeFrame.TimeFrame / 2));
            var end = Time.AddMinutes(Math.Ceiling((double)(int)TimeFrame.TimeFrame / 2));
            var Bar = TimeFrame.Bars.Where(b => start < b.Time && b.Time < end).FirstOrDefault();
            if (Bar == null)
                return new TradeResult() { Executed = false };
            var AddSubstract = (CurrencyPair.pSpread * CurrencyPair.pip) / 2;
            if (Position == Models.Enums.Position.Long)
            {
                AddSubstract = -AddSubstract;
            }
            return new TradeResult()
            {
                Executed = true,
                Price = ((Bar.Close + Bar.Open) / 2) + AddSubstract,
                Time = Time
            };
        }
    }
}
