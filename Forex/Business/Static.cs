﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forex.Business
{
    public class Static
    {
        public static decimal NeutralRange = (decimal).025;
        public static int LookAhead = 3;
        public static int ClipLength = 10;
        //public static double DeviationPercent = .25;
        public static int OpenCloseRange = 500;
        public static int RangeRange = 500;
        public static int MovementRange = 2500;
        public static int ThreadPredict = 100;
        public static int ScaleSize = 10000;
        public static int SMARange = 50;
        public static int TradeThreshold = 3;
        public static int Previous_Day_Range_StopLoss_Percent = 10;
        public static int DayofWeek_SafeBet = 2;
        public static int StopLossTradeValue = 100;
        public static List<Models.CurrencyPair> _CurrencyPairs { get; set; }
        public static List<Models.CurrencyPair> CurrencyPairs
        {
            get
            {
                if (_CurrencyPairs != null)
                    return _CurrencyPairs;

                var db = new Data.Context();
                _CurrencyPairs = db.CurrencyPairs.ToList();
                return _CurrencyPairs;
            }
        }
        public static int GetIntervalLength(Models.Enums.TimeFrame TimeFrame)
        {
            return (int)TimeFrame;
        }
        public static int GetStopLossLookBack(Models.Enums.TimeFrame TimeFrame)
        {
            return 3;
            switch (TimeFrame)
            {
                case Models.Enums.TimeFrame.D:
                case Models.Enums.TimeFrame.H4:
                case Models.Enums.TimeFrame.H1:
                case Models.Enums.TimeFrame.M30:
                    return 1;
                
                case Models.Enums.TimeFrame.M15:
                    return 2;
                
                case Models.Enums.TimeFrame.M10:
                case Models.Enums.TimeFrame.M5:
                    return 3;
                
                case Models.Enums.TimeFrame.M1:
                    return 5;
            }
            return 0;
        }
    }
}
