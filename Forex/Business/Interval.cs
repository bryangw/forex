﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Forex.Models;
namespace Forex.Business
{
    public delegate void NewBarHandler(Interval Interval);
    public class Interval
    {
        public event NewBarHandler NewBar;
        public List<Bar> Bars { get; set; }
        public List<Bar> PendingBars { get; set; }
        public List<Indicators.Indicator> Indicators { get; set; }

        public Enums.TimeFrame TimeFrame { get; set; }
        public int IntervalLength { get; set; }
        public DateTime CurrentDateTime { get; set; }
        public Models.Trade Trade { get; set; }
        public Models.CurrencyPair CurrencyPair { get; set; }
        public Connectors.ConnectorBase Connector { get; set; }
        public Interval(int IntervalLength, Models.CurrencyPair CurrencyPair, Connectors.ConnectorBase Connector)
        {
            Construct(IntervalLength, CurrencyPair, Connector);
        }
        public Interval(Enums.TimeFrame TimeFrame, Models.CurrencyPair CurrencyPair, Connectors.ConnectorBase Connector)
        {
            Construct((int)TimeFrame, CurrencyPair, Connector);
        }
        public void Construct(int IntervalLength, Models.CurrencyPair CurrencyPair, Connectors.ConnectorBase Connector)
        {
            this.Connector = Connector;
            this.IntervalLength = IntervalLength;
            this.TimeFrame = (Enums.TimeFrame)this.IntervalLength;
            this.CurrencyPair = CurrencyPair;

            Bars = new List<Bar>();
            PendingBars = new List<Bar>();
            Indicators = new List<Indicators.Indicator>();
        }
        public DateTime GetCurrentDateTime(Bar bar)
        {
            return new DateTime((long)Math.Floor((decimal)bar.Time.Ticks / ((int)IntervalLength * TimeSpan.TicksPerMinute)) * (IntervalLength * TimeSpan.TicksPerMinute));
        }
        public void React(int TradeValue)
        {
            var Position = TradeValue == Math.Abs(TradeValue) ? Enums.Position.Short : Enums.Position.Long;
            var Time = this.PendingBars.Count() > 0 ? this.PendingBars[this.PendingBars.Count()-1].Time : this.Bars[this.Bars.Count() - 1].Time;
            if (Trade != null && Position != Trade.Position)
            {
                var db = new Data.Context();
                    Trade.ClosePosition(Time);
                db.Trades.Add(Trade);
                db.SaveChanges();
                Trade = null;
                return;
            }
            
            Trade = new Trade(this, Position, Connector);
            if (!Trade.Execute(Time))
                Trade = null;
        }
        public void PushBar(Bar bar)
        {
            var TradeValue = 0;
            if (CurrentDateTime == default(DateTime))
            {
                CurrentDateTime = GetCurrentDateTime(bar);
            }
            if (CurrentDateTime.AddMinutes((int)IntervalLength) > bar.Time)
            {
                PendingBars.Add(bar);
                if (Trade == null || !Trade.HitStopLoss(bar.Close)) { return; }
                TradeValue = Trade.Position == Enums.Position.Long ? -Business.Static.StopLossTradeValue : Business.Static.StopLossTradeValue;
                //React(TradeValue);
                return;
            }

            
            var newBar = new Bar() { Time = CurrentDateTime };

            newBar.Close = PendingBars[PendingBars.Count() - 1].Close;
            newBar.Open = PendingBars[0].Open;
            newBar.High = PendingBars.OrderByDescending(b => b.High).First().High;
            newBar.Low = PendingBars.OrderBy(b => b.Low).First().Low;
            CurrentDateTime = CurrentDateTime.AddMinutes((int)IntervalLength);
            Bars.Add(newBar);

            if (Bars.Count > 1440)
                Bars.RemoveAt(0);

            
            PendingBars.Clear();
            PendingBars.Add(bar);
            

            if (Trade != null)
                Trade.SetStopLoss();

            TradeValue = 0;

            foreach (var i in Indicators)
                TradeValue += i.PushBar(newBar);

            if (Math.Abs(TradeValue) >= Business.Static.TradeThreshold)
                React(TradeValue);

            if (NewBar != null)
            {
                NewBar(this);
            }
        }
    }
}
