﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Forex.Models;
namespace Forex.Business
{
    public class CurrencyPair
    {
        public Models.CurrencyPair dbCurrencyPair{get;set;}
        public Connectors.ConnectorBase Connector { get; set; }
        public List<Trade> Trades { get; set; }
        public List<Interval> Intervals { get; set; }
        public bool inTrade { get; set; }
        public CurrencyPair(Models.CurrencyPair Pair)
        {
            this.dbCurrencyPair = Pair;
            Intervals = new List<Interval>();
        }
       

        
    }
}
