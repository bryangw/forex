﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forex.Business
{
    public static class Shared
    {
        public static decimal CalculateEMA(IBarWithMovingAverage PreviousBar, IBarWithMovingAverage CurrentBar, int count)
        {
            decimal multiplier = 2 / ((decimal)count + 1);
            return CurrentBar.Close * multiplier + PreviousBar.MovingAverage * (1 - multiplier);
        }
        public static decimal CalculateSMA<T>(IList<T> Bars) where T : IBar
        {
            return Bars.Sum(b => b.Close) / (decimal)Bars.Count();
        }
        public static int CalculateSMA(List<Forex.Models.Bar> Bars)
        {
            return (int)Math.Round(Bars.Sum(b => b.Movement) / (decimal)Bars.Count(), MidpointRounding.ToEven);
        }
        public static decimal CalculateATR(IBarWithATR PreviousBar, IBarWithATR CurrentBar)
        {
            var HighMinusLow = CurrentBar.High - CurrentBar.Low;
            var HighLessPreviousClose = Math.Abs(CurrentBar.High - PreviousBar.Close);
            var LowLessPreviousClose = Math.Abs(CurrentBar.Low - PreviousBar.Close);
            
            var ATR = HighMinusLow;
            if (HighLessPreviousClose > ATR) { ATR = HighLessPreviousClose; }
            if (LowLessPreviousClose > ATR) { ATR = LowLessPreviousClose; }
            return ATR;
        }
        public static Data.Context GetDB () {
            var db = new Data.Context();
            db.Configuration.AutoDetectChangesEnabled = false;
            db.Configuration.LazyLoadingEnabled = false;
            db.Configuration.ProxyCreationEnabled = false;
            db.Configuration.UseDatabaseNullSemantics = false;
            db.Configuration.ValidateOnSaveEnabled = false;
            return db;
        }
    }
}
