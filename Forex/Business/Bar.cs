﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forex.Business
{

    public class Bar
    {
        public decimal Open { get; set; }
        public decimal Low { get; set; }
        public decimal High { get; set; }
        public decimal Close { get; set; }
        public DateTime Time { get; set; }
    }
    public class KeltnerBar : Bar, IBarWithMovingAverage, IBarWithATR
    {
        public decimal MovingAverage { get; set; }
        public decimal ATR { get; set; }
        public decimal KeltnerPlus2 { get; set; }
        public decimal Keltner { get; set; }
    }
    public interface IBarWithMovingAverage: IBar
    {
        decimal MovingAverage { get; set; }
    }
    public interface IBarWithATR : IBar
    {
        decimal ATR { get; set; }
    }
    public interface IBar
    {
        decimal Open { get; set; }
        decimal Low { get; set; }
        decimal High { get; set; }
        decimal Close { get; set; }
    }
}
