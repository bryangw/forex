﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
namespace Forex.Models.Indicators
{
    public class DayofWeek : EntityBase
    {
        public Enums.DaysOfWeek DayOfWeek { get; set; }
        public Guid CurrencyPairId { get; set; }
        public CurrencyPair CurrencyPair { get; set; }
        public decimal Up { get; set; }
        public decimal Down { get; set; }
        public decimal StandardDeviation { get; set; }
        public decimal SafeBet { get; set; }
        [NotMapped]
        public decimal Mean { get; set; }
        [NotMapped]
        public decimal Direction { get; set; }
    }
}
