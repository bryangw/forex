﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forex.Models
{
    public class Quote : EntityBase
    {
        public Guid CurrencyPairId { get; set; }
        public CurrencyPair CurrencyPair { get; set; }
        public DateTime Time { get; set; }
        public decimal Open { get; set; }
        public decimal High { get; set; }
        public decimal Low { get; set; }
        public decimal Close { get; set; }
        public int Volume { get; set; }
        public bool isInsert { get; set; }
        public static implicit operator Forex.Business.Bar(Quote q)
        {
            return new Forex.Business.Bar()
            {
                Open = q.Open,
                Close = q.Close,
                High = q.High,
                Low = q.Low,
                Time = q.Time
            };
        }
    }
}
