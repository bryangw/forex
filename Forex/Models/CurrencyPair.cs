﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace Forex.Models
{
    public class CurrencyPair : EntityBase
    {
        [MaxLength(6)]
        public string Pair { get; set; }
        public decimal pip { get; set; }
        public decimal cSpread { get; set; }
        public decimal pSpread { get; set; }
        public virtual HashSet<Quote> Quotes { get; set; }
        public virtual HashSet<Trade> Trades { get; set; }
    }
}
