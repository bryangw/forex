﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forex.Models
{
    public class BarSet : EntityBase
    {
        public BarSet()
        {
            Bars = new List<Bar>();
        }
        public Guid CurrencyPairId { get; set; }
        public CurrencyPair CurrencyPair { get; set; }
        public Enums.TimeFrame TimeFrame { get; set; }
        public List<Bar> Bars { get; set; }
    }
    public class Bar : EntityBase
    {
        public Guid? BarSetId { get; set; }
        public BarSet BarSet { get; set; }

        public Guid PreviousBarId { get; set; }
        public Guid NextBarId { get; set; }

        public double Scale { get; set; }
        public int Open { get; set; }
        public int Close { get; set; }
        public int Range { get; set; }
        public int Movement { get; set; }
        public int SMA { get; set; }
    }
}
