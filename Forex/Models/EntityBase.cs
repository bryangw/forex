﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
namespace Forex.Models
{
    public class EntityBase
    {
        [Key]
        public Guid Id { get; set; }
    }
}
