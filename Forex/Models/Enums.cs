﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forex.Models
{
    public class Enums
    {
        public enum TrendType
        {
            Down = 0,
            Neutral = 1, 
            Up = 2
        }
        public enum TimeFrame
        {
            M1 = 1,
            M5 = 5, 
            M10 = 10,
            M15 = 15,
            M30 = 30,
            H1 = 60,
            H4 = 240,
            D = 1440
        }
        public enum BarValueType
        {
            Value = 1, 
            Move = 2
        }
        public enum DaysOfWeek
        {
            Sunday = 0,
            Monday = 1, 
            Tuesday = 2,
            Wednesday = 3, 
            Thursday = 4, 
            Friday = 5,
            Saturday = 6
        }
        public enum Position
        {
            None = 0,
            Long = 1, 
            Short = 2
        }
        public enum IndicatorType
        {
            Indicator = 1, 
            Stoploss = 2
        }
    }
}
