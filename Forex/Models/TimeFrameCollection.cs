﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forex.Models
{
    public class TimeFrameCollection : EntityBase
    {
        public TimeFrameCollection()
        {
            Bars = new List<Business.Bar>();
        }
        public Guid PairId { get; set; }
        public string Symbol { get; set; }
        public CurrencyPair Pair { get; set; }
        public Enums.TimeFrame TimeFrame { get; set; }
        public string sTimeFrame { get; set; }
        public List<Business.Bar> Bars { get; set; }
    }
}
