﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forex.Models
{
    public class BarTest : EntityBase
    {
        public int InitialSampleSize { get; set; }
        public int SampleSize { get; set; }
        public Guid PredictedBarId { get; set; }
        public Bar PredictedBar { get; set; }
        public Guid ActualBarId { get; set; }
        public Bar ActualBar { get; set; }
        public int Foresight { get; set; }
        public int Hindsight { get; set; }
    }
}
