﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
namespace Forex.Models
{
    public class Trade : EntityBase
    {
        public Guid CurrencyPairId { get; set; }
        public CurrencyPair CurrencyPair { get; set; }
        public DateTime Opened { get; set; }
        public DateTime? Closed { get; set; }
        public decimal OpenPrice { get; set; }
        public decimal ClosePrice { get; set; }
        public decimal StopLoss { get; set; }
        public decimal TakeProfit { get; set; }
        public decimal Size { get; set; }
        public decimal Pips { get; set; }
        [NotMapped]
        public DateTime LastStopLossAdjust { get; set; }
        [NotMapped]
        public ulong Deal { get; set; }
        [NotMapped]
        public ulong Order { get; set; }
        [NotMapped]
        public uint Request_id { get; set; }
        [NotMapped]
        public uint RetCode { get; set; }
        [NotMapped]
        public MtApi5.MqlTradeRequest Request { get; set; }
        public Enums.Position Position { get; set; }
        [NotMapped]
        public Connectors.ConnectorBase Connector { get; set; }
        [NotMapped]
        public bool Executed { get; set; }
        [NotMapped]
        public Forex.Business.Interval Interval { get; set; }
        [NotMapped]
        public double Price { get; set; }
        [NotMapped]
        public string Symbol { get; set; }
        public Trade() { }
        public Trade(Forex.Business.Interval Interval, Enums.Position Position, Connectors.ConnectorBase Connector){
            this.Id = Guid.NewGuid();
            this.Interval = Interval;
            this.Position = Position;
            this.Connector = Connector;
            this.CurrencyPairId = Interval.CurrencyPair.Id;
        }
        public bool Execute(DateTime Time = default(DateTime))
        {
            if (CanCreateStopLoss() == false)
                return Executed = false;
            var Result = Connector.Execute(Interval.CurrencyPair.Pair, Size, Position, Time);
            if (Result.Executed == false)
                return Executed = false;
            Executed = true;
            Opened = Result.Time;
            OpenPrice = Result.Price;
            SetStopLoss();
            return true;
        }
        public bool ClosePosition(DateTime Time = default(DateTime))
        {
            if (this.Executed == false)
                return true;
            
            var Position = this.Position == Enums.Position.Long ? Enums.Position.Short : Enums.Position.Long;
            var Result = Connector.Execute(this.Interval.CurrencyPair.Pair, this.Size, this.Position, Time);
            if (Result.Executed == false)
                return false;
            Closed = Result.Time;
            ClosePrice = Result.Price;
            return true;
        }
        public bool CanCreateStopLoss()
        {
            var LookBack = Business.Static.GetStopLossLookBack(Interval.TimeFrame);
            if (Interval.Bars.Count() < LookBack)
                return false;
            return true;
        }
        public void SetStopLoss()
        {
            var LookBack = Business.Static.GetStopLossLookBack(Interval.TimeFrame);
            if (Interval.Bars.Count() < LookBack)
            {
                StopLoss = 0;
            }
            var LookBackBars = Interval.Bars.OrderByDescending(b => b.Time).Take(LookBack).ToList();
            var Range = LookBackBars.OrderByDescending(b => b.High).First().High - LookBackBars.OrderBy(b => b.Low).First().Low;
            Range = Range * (Business.Static.Previous_Day_Range_StopLoss_Percent / 100);
            Range = 0;
            if (Position == Enums.Position.Long)
                StopLoss = LookBackBars.OrderBy(b => b.Low).First().Low - Range;
            else
                StopLoss = LookBackBars.OrderByDescending(b => b.High).First().High + Range;
        }
        public bool HitStopLoss(decimal Price)
        {
            if(Position == Enums.Position.Long)
                return Price < StopLoss;
            
            if (Position == Enums.Position.Short)
                return Price > StopLoss;
            
            return false;
        }
    }
}
