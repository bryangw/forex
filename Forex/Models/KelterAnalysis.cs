﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forex.Models
{
    public class KelterAnalysis : EntityBase
    {
        public Guid CurrencyPairId { get; set; }
        public CurrencyPair CurrencyPair { get; set; }
        public decimal MovingAverage { get; set; }
        public decimal OpenDistance { get; set; }
        public decimal HighDistance {get;set;}
        public decimal LowDistance { get; set; }
        public decimal CloseDistance { get; set; }
        public decimal Open { get; set; }
        public decimal High { get; set; }
        public decimal Low { get; set; }
        public decimal Close { get; set; }
        public decimal Bar1Close { get; set; }
        public decimal Bar2Close { get; set; }
        public decimal Bar3Close { get; set; }
        public decimal Bar4Close { get; set; }
        public decimal Bar5Close { get; set; }
        public decimal ATR { get; set; }
        public bool isUpBar { get; set; }
        public bool is1BarUp { get; set; }
        public bool is2BarUp { get; set; }
        public bool is3BarUp { get; set; }
        public int Volume { get; set; }
        
    }
}
