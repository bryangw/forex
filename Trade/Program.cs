﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trade.Models;

namespace Trade
{
    class Program
    {
        public static DateTime LastUpdated { get; set; }
        public static Dictionary<string, Models.Trade> Trades { get; set; }
        public static List<Models.Trade> ClosedTrades { get; set; }
        public static Dictionary<string, Metrics> SymbolMetrics { get; set; }
        public static List<string> Pairs { get; set; }
        public static Connectors.IConnector Connector { get; set; }
        static double StopLoss = 100;
        static double TrailingStop = 100;
        static double TakeProfit = 10;
        static double Volume = .05;
        static uint Order = 0;
        static int TotalTrades = 0;
        static bool TakeProfitEnabled = false;
        static bool isUpdating = false;
        static int y;
        static Enums.Periods Period = Enums.Periods.M5;
        static double WaitTime = 30;  //360
        static Dictionary<string, List<ConsoleString>> ConsoleStrings { get; set; }
        static int RateCount = 56;
        static bool Finished = false;
        static bool FirstIteration = true;
        static int TrendStrength = 5000;
        static int TrendVelocity = 10;
        static int TrendHistory = 20;
        static int OpenTrades = 0;
        static int pSarPeriod = 28;
        static int ADXPeriod = 14;
        static int StopLossTimeout = 0;
        static int VelocitySustained = 3;
        static int UpdatePeriodCount = 0;
        static bool bailout = false;
        static bool isPeriodBar = false;
        static DateTime LastClose;
        static int LargeAverageMultiplier = 3;
        static double TotalStop = -250;
        static int tStop = 250;
        static void Main(string[] args)
        {
            ConsoleStrings = new Dictionary<string, List<ConsoleString>>();
            ClosedTrades = new List<Models.Trade>();
            Trades = new Dictionary<string, Models.Trade>();
            SymbolMetrics = new Dictionary<string, Metrics>();
            Connector = new Connectors.mt5();
            Connector.LoadCache(Period);
            Connector.LoadCache(Enums.Periods.M1);
            LastUpdated = Connector.Now.AddSeconds(-5);
            do
            {
                if (Connector.isConnected)
                {
                    if (FirstIteration)
                        LoadQuotes();

                    Finished = Finished || Connector.Next();
                    if (!Finished)
                        Update();
                }

                if (Connector.isLive)
                    System.Threading.Thread.Sleep(1000);

            } while (true);
        }
        private static void CloseAllTrades()
        {
            OpenTrades = 0;
            LastClose = Connector.Now;
            Trades.Select(t => t.Value).ToList().ForEach(t => CloseTrade(t, SymbolMetrics[t.Symbol].LastPrice, "Close All"));
        }
        private static void LoadQuotes()
        {
            FirstIteration = false;
            SymbolMetrics = new Dictionary<string, Metrics>();
            Connector.GetQuotes().ForEach(q =>
            {
                var m = new Metrics(q.Instrument, TrendHistory, TrendVelocity, TrendStrength, VelocitySustained);
                m.pSar = new PSar(Connector.GetRates(q.Instrument, Period, pSarPeriod));
                m.ADX = new ADX(Connector.GetRates(q.Instrument, Period, ADXPeriod), ADXPeriod);
                SymbolMetrics.Add(q.Instrument, m);

            });
        }
        private static Models.Trade OpenTrade(string Symbol, Enums.Position Position, double Volume, string Comment)
        {
            Comment = "Open " + Comment;

            var Metric = SymbolMetrics[Symbol];
            /*
            if (Metric.traded)
                return null;
            */
            var Trade = new Models.Trade() { Id = Guid.NewGuid(), Opened = Connector.Now, OpenPrice = Metric.LastPrice, Position = Position, Symbol = Symbol };
            if (Trade.Position == Enums.Position.Long)
            {
                Trade.StopLoss = Metric.LastPrice - (StopLoss * Metric.PipValue);
                Trade.TakeProfit = Metric.LastPrice + (TakeProfit * Metric.PipValue);
            }
            else
            {
                Trade.StopLoss = Metric.LastPrice + (StopLoss * Metric.PipValue);
                Trade.TakeProfit = Metric.LastPrice - (TakeProfit * Metric.PipValue);
            }

            Order++;

            if (!Connector.Execute(Symbol, Position, Volume, Comment))
                return null;
            Metric.traded = true;

            return Trade;
        }
        private static bool CloseTrade(Models.Trade Trade, double Price, string Comment)
        {

            var Metric = SymbolMetrics[Trade.Symbol];
            /*
            if (Connector.Now < Metric.TimeOut)
                return false;
            */

            var ClosePosition = Trade.Position == Enums.Position.Long ? Enums.Position.Short : Enums.Position.Long;

            var Pips = (double)(Trade.ClosePrice - Trade.OpenPrice) / Metric.PipValue;
            if (Trade.Position == Enums.Position.Short)
                Pips = -Pips;

            if (!Connector.Execute(Trade.Symbol, ClosePosition, Trade.Volume, Comment + " " + Pips))
                return false;
            Metric.TradePosition = Enums.Position.None;
            Trade.Closed = Connector.Now;
            Trade.ClosePrice = Price;
            Trades.Remove(Trade.Symbol);
            Metric.TimeOut = Connector.Now.AddMinutes(WaitTime);
            ClosedTrades.Add(Trade);
            Trade = null;
            return true;
        }

        private static ConsoleColor GetPositionColor(Enums.Position Position)
        {
            switch (Position)
            {
                case Enums.Position.None:
                    return ConsoleColor.White;

                case Enums.Position.Long:
                    return ConsoleColor.Green;

                case Enums.Position.Short:
                    return ConsoleColor.Red;

                case Enums.Position.Undefined:
                    return ConsoleColor.Gray;
            };
            return ConsoleColor.Gray;
        }
        private static ConsoleString CreateString(string Text, ConsoleColor Color, int Length)
        {
            return new ConsoleString()
            {
                Color = Color,
                Text = string.Format("{0," + Length + "}", Text)
            };
        }
        private static List<Models.ConsoleString> WriteSymbol(string Symbol)
        {
            Models.Trade Trade = null;
            if (Trades.ContainsKey(Symbol))
                Trade = Trades[Symbol];

            var Metrics = SymbolMetrics[Symbol];
            double Pips = 0;
            var TradePosition = Enums.Position.None;
            if (Trade != null)
            {
                TradePosition = Trade.Position;
                Pips = (Metrics.LastPrice - Trade.OpenPrice) / Metrics.PipValue;
                if (Trade.Position == Enums.Position.Short)
                    Pips = -Pips;
            }
            var retList = new List<Models.ConsoleString>();
            retList.Add(CreateString(Symbol, GetPositionColor(TradePosition), 6));

            retList.Add(CreateString(String.Format("{0:0}", Metrics.Trend), GetPositionColor(Metrics.TrendPosition), 8));
            retList.Add(CreateString(string.Format("{0:0}", Metrics.TrendVelocity), GetPositionColor(Metrics.TrendVelocityPosition), 6));
            if (Metrics.ADX.isPrimed)
                retList.Add(CreateString(string.Format("{0:0}", Metrics.ADX.value), GetPositionColor(Metrics.ADX.Position), 4));
            retList.Add(CreateString(String.Format("{0:0}",Metrics.Value), (Metrics.Value==Math.Abs(Metrics.Value) ? ConsoleColor.Green : ConsoleColor.Red),4));
            retList.Add(CreateString(string.Format("{0:0}", Math.Abs(Metrics.aValue)), GetPositionColor(Metrics.aPosition), 4));
            retList.Add(CreateString("PSAR", GetPositionColor(Metrics.pSar.Position), 6));
            retList.Add(CreateString("MA", GetPositionColor(Metrics.AveragesPosition), 4));
            var PipP = Pips > 0 ? Enums.Position.Long : Enums.Position.Short;
            var PipString = (Pips > 0 ? " " : "") + string.Format("{0:0.0}", Pips);
            if (Pips != 0)
                retList.Add(CreateString(PipString, GetPositionColor(PipP), 6));
            retList.Add(CreateString("\n", ConsoleColor.White, 1));
            return retList;
        }


        private static bool CheckStopLoss(Models.Trade Trade, Rate Bar)
        {
            var Metrics = SymbolMetrics[Trade.Symbol];

            if (Metrics.TimeOut < Connector.Now)
            {
                var pStopLoss = Metrics.PipValue * TrailingStop;

                if (Trade.Position == Enums.Position.Long)
                    Trade.StopLoss = Bar.Close - pStopLoss > Trade.StopLoss
                        ? Bar.Close - pStopLoss
                        : Trade.StopLoss;
                else
                    Trade.StopLoss = Bar.Close + pStopLoss < Trade.StopLoss
                        ? Bar.Close + pStopLoss
                        : Trade.StopLoss;

            }
            var value = 0;



            
            //value += Metrics.pSar.Position != Trade.Position && isDefinitivePosition(Metrics.pSar.Position) ? -1 : 0;

           //value += Metrics.ADX.Position != Trade.Position && isDefinitivePosition(Metrics.ADX.Position) ? -1 : 0;
            /*if(isDefinitivePosition(Metrics.TrendVelocityPosition))
                value += Metrics.TrendVelocityPosition == Trade.Position ? 0 : -1;
            */
            /*
            if (isDefinitivePosition(Metrics.MASellPosition))
                value += Metrics.MASellPosition != Trade.Position ? -1 : 0;
              /*
             if(isDefinitivePosition(Metrics.TrendVelocityPosition))
            value += Metrics.TrendVelocityPosition != Trade.Position ? -1 : 0;
             */
            /*
            if (isDefinitivePosition(Metrics.TrendPosition))
                value += Metrics.TrendPosition == Trade.Position ? 1 : -1;
            if (isDefinitivePosition(Metrics.aPosition))
                value += Metrics.aPosition == Trade.Position ? 1 : -2;
             * */
            
            if(isDefinitivePosition(Metrics.AveragesPosition))
            value += Metrics.AveragesPosition != Trade.Position ? -1 : 0;
            
            value += Trade.Position == Enums.Position.Long && Trade.StopLoss > Bar.Close ? -5 : 0;
            value += Trade.Position == Enums.Position.Short && Trade.StopLoss < Bar.Close ? -5 : 0;

            var sl = value < 0;
            if (sl)
                SymbolMetrics[Trade.Symbol].TimeOut = Connector.Now.AddMinutes(WaitTime);
            return sl;
        }
        private static bool CheckTakeProfit(Models.Trade Trade, Rate Bar)
        {
            return ((Trade.Position == Enums.Position.Long && Trade.TakeProfit < Bar.Close && TakeProfitEnabled)
                        || (Trade.Position == Enums.Position.Short && Trade.TakeProfit > Bar.Close && TakeProfitEnabled));
        }
        private static void Update()
        {

            ConsoleStrings.Clear();
            if (isUpdating || Finished) { return; }
            isPeriodBar = false;
            if (UpdatePeriodCount > (int)Period)
            {
                isPeriodBar = true;
                UpdatePeriodCount = 0;
            }
            isUpdating = true;
            LastUpdated = Connector.Now;

            var Quotes = Connector.GetQuotes();
            var tw = new System.IO.StringWriter();

            if (!Connector.isLive)
            {
                var Threads = new List<System.Threading.Thread>();
                foreach (var q in Quotes.OrderBy(q => q.Instrument).ToList())
                {
                    var newThread = new System.Threading.Thread(UpdateQuote);
                    newThread.Start(q);
                    Threads.Add(newThread);
                }
                do
                {
                    System.Threading.Thread.Sleep(50);
                } while (Threads.Any(t => t.ThreadState == System.Threading.ThreadState.Running));
            }
            else
            {
                foreach (var q in Quotes.OrderBy(q => q.Instrument).ToList())
                {
                    UpdateQuote(q);
                }
            }
            double TotalPips = 0;
            double OpenPips = 0;
            TotalPips -= TotalTrades * 3;
            OpenPips -= OpenTrades * 3;
            ClosedTrades.ToList().ForEach(t =>
            {
                var Metric = SymbolMetrics[t.Symbol];
                var Pips = (double)(t.ClosePrice - t.OpenPrice) / Metric.PipValue;
                if (t.Position == Enums.Position.Short)
                    Pips = -Pips;

                TotalPips += Pips;
                if (t.Opened > LastClose)
                    OpenPips += Pips;
            });
            Trades.Select(t => t.Value).ToList().ForEach(t =>
            {
                var Metric = SymbolMetrics[t.Symbol];

                var Pips = (SymbolMetrics[t.Symbol].LastPrice - t.OpenPrice) / Metric.PipValue;
                if (t.Position == Enums.Position.Short)
                    Pips = -Pips;
                t.Pips = Pips;
                TotalPips += Pips;
                if (t.Opened > LastClose)
                    OpenPips += Pips;
            });
            /*
            
            if (OpenPips > 100)
            {
                bailout = false;
                CloseAllTrades();
                SymbolMetrics.ToList().ForEach(sm => sm.Value.TimeOut = Connector.Now.AddHours(.5));
                do
                {
                    System.Threading.Thread.Sleep(1000);
                } while (true);
            }
            */
            var TotalPosition = TotalPips > 0 ? Enums.Position.Long : Enums.Position.Short;
            var OpenPosition = OpenPips > 0 ? Enums.Position.Long : Enums.Position.Short;
            var Ordered = new List<ConsoleString>();
            ConsoleStrings.OrderBy(cs => cs.Key).Select(cs => cs.Value).ToList().ForEach(cs => Ordered.AddRange(cs));
            Ordered.Add(CreateString("Open: " + String.Format("{0:0.0}", OpenPips) + "\n", GetPositionColor(OpenPosition), 0));
            Ordered.Add(CreateString("Total: " + String.Format("{0:0.0}", TotalPips) + "\n", GetPositionColor(TotalPosition), 0));
            Ordered.Add(CreateString("Trades: " + String.Format("{0:0}", TotalTrades) + "\n", ConsoleColor.White, 0));
            Ordered.Add(CreateString(Connector.Now.ToShortDateString() + " " + Connector.Now.ToShortTimeString(), ConsoleColor.White, 0));
            Console.Clear();
            Ordered.ForEach(cs =>
            {
                Console.ForegroundColor = cs.Color;
                Console.Write(cs.Text);
            });
            UpdatePeriodCount++;
            isUpdating = false;

        }
        public static bool isPosition(Enums.Position Position)
        {
            return Position != Enums.Position.Undefined;
        }
        public static bool isDefinitivePosition(Enums.Position Position)
        {
            return Position != Enums.Position.Undefined && Position != Enums.Position.None;
        }
        public static void UpdateQuote(object ObjQuote)
        {
            var q = (Quote)ObjQuote;
            double Value = 0;


            Models.Trade Trade = null;
            var Metrics = SymbolMetrics[q.Instrument];
            if (Trades.ContainsKey(q.Instrument))
                Trade = Trades[q.Instrument];

            var Rates = Connector.GetRates(q.Instrument, Period, RateCount);
            if (Rates.Count() == 0)
            {
                Finished = true;
                return;
            }
            var Bar = Rates[Rates.Count() - 1];
            SymbolMetrics[q.Instrument].LastPrice = Bar.Close;
            if (isPeriodBar)
            {
                Metrics.pSar.Update(Bar);
                Metrics.ADX.Update(Bar);
            }


            var Volatile = true;  //Math.Abs((Rates.OrderByDescending(r => r.High).First().High - Rates.OrderBy(r => r.Low).First().Low) / PipValue) > Volatality;
            double sValue = 0;
            var count = Rates.Count();
            var aValue = 0;
            for (var x = count - 1; x > count - count / 2; x--)
                for (var y = x - 1; y > x - count / 2; y--)
                    Value += (Rates[x].Close > Rates[y].Close ? 1 : -1) * (Math.Abs(Rates[x].Close - Rates[y].Close) / Metrics.PipValue); //* x - (count/2);
            for (var x = count - 2; x > count / 2; x--)
                aValue += Rates[x].Close > Rates[x - 1].Close ? 1 : -1;
            Metrics.SmallAverage = Rates.Sum(r => r.Close) / count;
            var lRates = Connector.GetRates(q.Instrument, Period, count * LargeAverageMultiplier);
            Metrics.LargeAverage = lRates.Sum(r => r.Close) / lRates.Count();
            var aPosition = Enums.Position.None;
            if (Math.Abs(aValue) > 7)
                aPosition = aValue == Math.Abs(aValue) ? Enums.Position.Long : Enums.Position.Short;
            Metrics.aPosition = aPosition;
            Metrics.aValue = aValue;
            Metrics.Trend = (int)Math.Round(Value);
            Metrics.AveragesPosition = Metrics.SmallAverage > Metrics.LargeAverage ? Enums.Position.Long : Enums.Position.Short;
               
            lock (ConsoleStrings)
                ConsoleStrings.Add(q.Instrument, WriteSymbol(q.Instrument));

            if (Trade != null)
            {
                var StopLoss = CheckStopLoss(Trade, Bar);
                var TakeProfit = CheckTakeProfit(Trade, Bar);

                if (StopLoss || TakeProfit)
                {
                    CloseTrade(Trade, Bar.Close, (StopLoss ? "Stop Loss" : "Take Profit"));
                    return;
                }
            }

            //----------------------------------------------------------------------
            var value = 0;
            /*
            if (isDefinitivePosition(Metrics.TrendVelocityPosition))
                value += Metrics.TrendVelocityPosition == Enums.Position.Long ? 1 : -1;
            /*
            if (isDefinitivePosition(Metrics.ADX.Position)) 
                value += Metrics.ADX.Position == Enums.Position.Long ? 1 : -1;
            
            if (Math.Abs(Metrics.Value) > 150)
                value += Metrics.Value == Math.Abs(Metrics.Value) ? 1 : -1;
            */
            if (isDefinitivePosition(Metrics.pSar.Position))
                value += Metrics.pSar.Position == Enums.Position.Long ? 1 : -1;
            if (isDefinitivePosition(Metrics.AveragesPosition))
                value += Metrics.AveragesPosition == Enums.Position.Long ? 1 : -1;
            var tPosition = Enums.Position.None;
            if (Math.Abs(value) >=2)
                tPosition = value == Math.Abs(value) ? Enums.Position.Long : Enums.Position.Short;

            var doOpenTrade = Trade == null && tPosition != Enums.Position.None;


            var doCloseTrade = Trade != null
                && (isPosition(Metrics.TrendPosition) && Metrics.TrendPosition != Enums.Position.Short && Trade.Position == Enums.Position.Short)
                && (isPosition(Metrics.TrendPosition) && Metrics.TrendPosition != Enums.Position.None && Trade.Position == Enums.Position.Long);


            if (!doOpenTrade && !doCloseTrade)
                return;

            if (doOpenTrade && Trade == null)
            {

                if (SymbolMetrics[q.Instrument].TimeOut > Connector.Now)
                    return;
                Trade = OpenTrade(Metrics.Symbol, tPosition, Volume, "Trend: " + string.Format("{0:0}", Metrics.Trend) + " Velocity: " + string.Format("{0:0}", Metrics.TrendVelocity));


                if (Trade == null)
                    return;

                Metrics.TradePosition = Trade.Position;
                Metrics.TimeOut = Connector.Now.AddMinutes(StopLossTimeout);
                TotalTrades += 1;
                OpenTrades += 1;
                lock (Trades)
                    Trades.Add(q.Instrument, Trade);
                return;
            }

        }
    }
}
