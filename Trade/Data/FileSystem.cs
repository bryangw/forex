﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trade.Models;

namespace Trade.Data
{
    public static class FileSystem
    {
        public static string Path = "C:\\Data\\Forex\\";
        public static List<Rate> GetRates(string Symbol, Enums.Periods Period)
        {

            var File= Path + Symbol + " " + Period.ToString() + ".csv";
            var engine = new FileHelpers.FileHelperEngine(typeof(Trade.Models.Rate));
            var Rates = (Rate[])engine.ReadFile(File);
            var retList = new List<Rate>();
            retList.AddRange(Rates);
                        return retList;
        }
        public static void SaveRates(string Symbol, Enums.Periods Period, List<Rate> Rates)
        {
            var sw = new System.IO.StreamWriter(Path + Symbol + " " + Period.ToString() + ".json");
            sw.Write(Newtonsoft.Json.JsonConvert.SerializeObject(Rates));
            sw.Close();
        }
        public static void WriteList(string FileName, List<string> Strings)
        {
            if (System.IO.File.Exists(Path + FileName + ".txt"))
                System.IO.File.Delete(Path + FileName + ".txt");

            var sw = new System.IO.StreamWriter(Path + FileName + ".txt");
            Strings.ForEach(s => sw.WriteLine(s));
            sw.Close();
        }
        public static List<string> ReadList(string FileName)
        {
            var retList = new List<string>();

            if (!System.IO.File.Exists(Path + FileName + ".txt"))
                return retList;
            
            var sr = new System.IO.StreamReader(Path + FileName + ".txt");
            
            do
                retList.Add(sr.ReadLine());
            while (!sr.EndOfStream);
            
            return retList;

        }
    }
}
