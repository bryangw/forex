﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trade.Models
{
    public class Enums
    {
        public enum Position
        {
            Undefined = 0,
            None = 1,
            Long = 2, 
            Short = 3
        }
        public enum Periods
        {
            M1 = 1,
            M2 = 2, 
            M3 = 3,
            M4 = 4,
            M5 = 5,
            M6 = 6,
            M10 = 10,
            M12 = 12,
            M15 = 15, 
            M20 = 20,
            M30 = 30,
            H1 = 60,
            H2 = 120,
            H3 = 180,
            H4 = 240,
            H6 = 360, 
            H8 = 380,
            H12 = 720,
            D1 = 1440,
            W1 = 10080
        }
    }
}
