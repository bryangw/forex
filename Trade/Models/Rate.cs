﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;
namespace Trade.Models
{
    [DelimitedRecord(",")]
    public class Rate
    {
        [FieldConverter(typeof(Misc.DateTimeConverter))]
        public DateTime Time;
        public double Open;
        public double High;
        public double Low;
        public double Close;
    }
}
