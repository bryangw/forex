﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trade.Models
{
    public class ConsoleString
    {
        public string Text { get; set; }
        public ConsoleColor Color { get; set; }
    }
}
