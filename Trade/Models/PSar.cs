﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trade.Models
{

    public class PSar
    {
        public Enums.Position TrendPosition = Enums.Position.Short;
        public double AF = .02;
        public double cAF = .02;
        public double max = .2;
        public double Value = 0;
        public double EP = 0;
        public List<Rate> Values;
        public int Period;
        public int count = 0;
        public int verificationcount = 5;
        public bool first = true;
        public Enums.Position Position
        {
            get
            {
                if (count < verificationcount)
                    return Enums.Position.Undefined;

                return TrendPosition;
            }
        }

        public PSar(List<Rate> StartData, int Period = 14)
        {
            Values = new List<Rate>();
            this.Period = Period;
            EP = StartData[0].Close;
            foreach (var i in StartData)
            {
                Update(i);
            }
        }
        public void Reset(Enums.Position Position)
        {
            
            
            this.TrendPosition = Position;
            cAF = .02;
            count = 0;
            switch (Position)
            {
                
                case Enums.Position.Short:
                    Value = Values.Skip(Period-2).OrderByDescending(r => r.High).First().High;
                    EP = Values.OrderBy(r => r.Low).First().Low;
                    break;
                case Enums.Position.Long:
                    Value = Values.Skip(Period-2).OrderBy(r => r.Low).First().Low;
                    EP = Values.OrderByDescending(r => r.High).First().High;
                    break;
            }
        }
        public void Update(Rate Rate)
        {

            count++;
            Values.Add(Rate);
            if (Values.Count() < Period)
                return;
            if (Values.Count() > Period)
                Values.RemoveAt(0);
            if (first)
            {
                Value = Values.Skip(Period - 2).OrderByDescending(r => r.High).First().High;
                EP = Values.OrderBy(r => r.Low).First().Low;
                first = false;
            }

            var diff = cAF * (EP - Value);

            switch (TrendPosition)
            {
                case Enums.Position.Long:
                    if (EP < Rate.High)
                    {
                        EP = Rate.High;
                        cAF += AF;
                        //count++;
                    }
                    Value += diff;
                        
                    if (Rate.Low < Value)
                        Reset(Enums.Position.Short);

                    break;

                case Enums.Position.Short:
                    if (EP > Rate.Low)
                    {
                        EP = Rate.Low;
                        cAF += AF;
                        //count++;
                    }
                    Value += diff;
                     

                    if (Rate.High > Value)
                        Reset(Enums.Position.Long);

                    break;
            }
            if (count > verificationcount)
                count = verificationcount;
            if (cAF > max)
                AF = max;
            
        }
    }
}
