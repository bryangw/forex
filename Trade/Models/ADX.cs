﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trade.Models
{

    public class ADX
    {
        public List<double> Plus { get; set; }
        public List<double> Minus { get; set; }
        public List<double> lADX { get; set; }
        public List<double> ATR { get; set; }
        public Enums.Position Position { get; set; }
        public double pDM { get; set; }
        public double nDM { get; set; }
        public double adx { get; set; }
        public double lastpDi { get; set; }
        public double lastnDi { get; set; }
        public double lastAdx { get; set; }
        public double lastATR { get; set; }
        public double value { get; set; }
        public int Period = 0;
        public bool isPrimed = false;
        public Rate LastBar { get; set; }

        public ADX(List<Rate> StartData, int Period)
        {
            this.Period = Period;
            this.Position = Enums.Position.Undefined;
            Plus = new List<double>();
            Minus = new List<double>();
            lADX = new List<double>();
            ATR = new List<double>();
            for (var x = 0; x < StartData.Count(); x++)
                Update(StartData[x]);
        }
        public void Update(Rate Rate)
        {
            if (lADX.Count() < Period)
            {
                Prime(Rate);
                return;
            }
            if (!isPrimed)
                SetPrimed(Rate);
            CalculateADX(Rate);
        }
        public void SetPrimed(Rate Rate)
        {
            lastpDi = CalculateSMA(Plus);
            lastnDi = CalculateSMA(Minus);
            lastAdx = Math.Abs(lastpDi - lastnDi) / (lastpDi + lastnDi);
            lastATR = CalculateSMA(ATR);
            isPrimed = true;
        }
        public void AddToArrays(double pDi, double nDi, double adx, double atr)
        {
            lADX.Add(adx);
            if (lADX.Count() > Period)
                lADX.RemoveAt(0);

            Plus.Add(pDi);
            if (Plus.Count() > Period)
                Plus.RemoveAt(0);

            Minus.Add(nDi);
            if (Minus.Count() > Period)
                Minus.RemoveAt(0);

            ATR.Add(atr);
            if (ATR.Count() > Period)
                ATR.RemoveAt(0);
        }
        public void CalculateADX(Rate Rate)
        {
            var pDi = Rate.High - LastBar.High;
            var nDi = LastBar.Low - Rate.Low;

            if (!(pDi > nDi && pDi > 0))
                pDi = 0;
            if (!(nDi > pDi && nDi > 0))
                nDi = 0;
            
            var emapDi = CalculateEMA(pDi, lastpDi, Period);
            var emanDi = CalculateEMA(nDi, lastnDi, Period);
            var calc = Math.Abs(emapDi - emanDi) / (emanDi + emapDi);
            calc = CalculateEMA(calc, lastAdx, Period);


            var ATR = CalculateATR(Rate, LastBar);
            ATR = CalculateEMA(ATR, lastATR, Period);
            lastATR = ATR;

            pDM = 100 *  (emapDi / ATR);
            lastpDi = emapDi;

            nDM = 100 *  (emanDi / ATR);
            lastnDi = emanDi;

            value = 100 * calc;
            lastAdx = calc;
            
            LastBar = Rate;


            if (value < 20 || pDM == nDM)
            {
                Position = Enums.Position.None;
                return;
            }

            Position = pDM > nDM ? Enums.Position.Long : Enums.Position.Short;
        }
        public void Prime(Rate Rate)
        {
            var doReturn = LastBar == null;
            if (doReturn)
            { LastBar = Rate; return; }
            var pDi = Rate.High - LastBar.High;
            var nDi = LastBar.Low - Rate.Low;

            if (!(pDi > nDi && pDi > 0))
                pDi = 0;
            if (!(nDi > pDi && nDi > 0))
                nDi = 0;
            var atr = CalculateATR(Rate, LastBar);
            var calc = Math.Abs(pDi - nDi) / (pDi + nDi);
            AddToArrays(pDi, nDi, calc, atr);
            LastBar = Rate;
        }
        public static double CalculateEMA(double Current, double PreviousMA, int count)
        {
            var multiplier = 2 / ((double)count + 1);
            return (Current - PreviousMA) * multiplier + PreviousMA;
        }
        public static double CalculateSMA(List<Double> Values)
        {
            return Values.Sum(v => v) / (double)Values.Count();
        }
        public static double CalculateATR(Rate Current, Rate Previous)
        {
            var HighMinusLow = Current.High - Current.Low;
            var HighLessPreviousClose = Math.Abs(Current.High - Previous.Close);
            var LowLessPreviousClose = Math.Abs(Current.Low - Previous.Close);

            var ATR = HighMinusLow;
            if (HighLessPreviousClose > ATR) { ATR = HighLessPreviousClose; }
            if (LowLessPreviousClose > ATR) { ATR = LowLessPreviousClose; }
            return ATR;
        }
    }
}
