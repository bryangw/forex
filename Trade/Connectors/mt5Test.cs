﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trade.Models;

namespace Trade.Connectors
{
    public class ThreadStartParameters
    {
        public Enums.Periods Period { get; set; }
        public string Symbol { get; set; }
    }
    public class mt5Test : IConnector
    {
        public bool isConnected { get { return true; } }
        public bool isLive { get { return false; } }
        private DateTime _Now { get; set; }
        public DateTime Now { get { return _Now; } }
        private int MinuteCount { get; set; }
        private List<Quote> Quotes { get; set; }

        private Dictionary<string, Dictionary<Enums.Periods, List<Rate>>> Cache { get; set; }

        public mt5Test(DateTime StartDate = default(DateTime))
        {
            if (StartDate == default(DateTime))
                StartDate = new DateTime(2015, 2, 16,11,0,0);
            _Now = StartDate;
            Quotes = new List<Quote>();
            Data.FileSystem.ReadList("Pairs").ForEach(s => Quotes.Add(new Quote() { Instrument = s }));
            Cache = new Dictionary<string, Dictionary<Enums.Periods, List<Rate>>>();
            Quotes.ForEach(q => Cache.Add(q.Instrument, new Dictionary<Enums.Periods, List<Rate>>()));
        }
        public List<Rate> Fetch(string Symbol, Enums.Periods Period)
        {
            if (Cache[Symbol].ContainsKey(Period))
                return Cache[Symbol][Period];

            Cache[Symbol].Add(Period, Data.FileSystem.GetRates(Symbol, Period));
            return Cache[Symbol][Period];
        }
        public List<Models.Quote> GetQuotes()
        {
            return Quotes;
        }
        public bool Next()
        {
            _Now = _Now.AddMinutes(1);
            if (_Now.Date.DayOfWeek == DayOfWeek.Thursday && _Now.Hour >= 22)
            {
                _Now = _Now.AddDays(3).AddHours(-22);
            }

            if (_Now > DateTime.Now.AddDays(-3))
            {
                return true;
            }
            return false;

        }
        public bool Execute(string Symbol, Models.Enums.Position Position, double Volume, string Comment = "")
        {
            return true;
        }
        public List<Models.Rate> GetRates(string Symbol, Models.Enums.Periods Period, int Count)
        {
            var Requests = Fetch(Symbol, Period).Where(r => r.Time <= _Now.AddMinutes(-(int)Period)).Reverse();
            var Requested = Requests.Take(Count - 1).Reverse().ToList();
            var Minute = Fetch(Symbol, Enums.Periods.M1).Where(r => r.Time <= _Now).Reverse().Take(1).First();

            Requested.Add(Minute);



            return Requested;
        }
        public void LoadCache(Models.Enums.Periods Period)
        {
            var Quotes = GetQuotes();
            var Threads = new List<System.Threading.Thread>();
            System.Threading.Thread newThread = null;
            for (var x = 0; x < Quotes.Count(); x++)
            {
                                  
                    newThread = new System.Threading.Thread(ThreadStart);
                    newThread.Start(new ThreadStartParameters() { Period = Period, Symbol = Quotes[x].Instrument });
                    Threads.Add(newThread);
                /*    
                newThread = new System.Threading.Thread(ThreadStart);
                    newThread.Start(new ThreadStartParameters() { Period = Enums.Periods.M1, Symbol = Quotes[x].Instrument });
                    Threads.Add(newThread);
                */
            }
            do
            {
                Console.Clear();
                Console.WriteLine(Threads.Count(t => t.ThreadState != System.Threading.ThreadState.Running) + "  " + Quotes.Count);
                System.Threading.Thread.Sleep(1000);
            } while (Threads.Any(t => t.ThreadState == System.Threading.ThreadState.Running));

        }
        public void ThreadStart(object ThreadStart)
        {
            var parameters = (ThreadStartParameters)ThreadStart;
            Fetch(parameters.Symbol, parameters.Period);
        }

    }
}
