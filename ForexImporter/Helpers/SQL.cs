﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForexImporter.Helpers
{
    public static class SQL
    {
        public static List<T> BulkRead<T>(string Command)
        {
            var Conn = new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Context"].ConnectionString);
            Conn.Open();
            var cmd = new System.Data.SqlClient.SqlCommand(Command, Conn);
            var Reader = cmd.ExecuteReader();
            List<T> RetList = AutoMapper.Mapper.DynamicMap<System.Data.IDataReader, List<T>>(Reader);
            return RetList;
        }
    }
}
