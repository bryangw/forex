﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;
namespace ForexImporter
{
    class DateTimeSQLConverter : ConverterBase
    {
        public override string FieldToString(object from)
        {
            var DateTimeObj = from as Nullable<DateTime>;
            if(DateTimeObj ==null){ return default(DateTime).ToString();}
            var DT = DateTimeObj.Value;
            return DT.Year.ToString() + "-" + DT.Month.ToString() + "-" + DT.Day.ToString() + " " + DT.Hour.ToString() + ":" + DT.Minute.ToString() + ":00.000";
        }
        public override object StringToField(string from)
        {
            throw new NotImplementedException();
        }
    }
}
