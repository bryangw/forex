﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;
namespace ForexImporter
{
    class GuidConverter : ConverterBase
    {
        public override string FieldToString(object from)
        {
            var GuidObj = from as Nullable<Guid>;
            if (!GuidObj.HasValue) { return ""; }
            return GuidObj.Value.ToString();
        }
        public override object StringToField(string from)
        {
            return Guid.Parse(from);
        }
    }
}
