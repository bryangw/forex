﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;
namespace ForexImporter
{
    [DelimitedRecord(",")]
    class QuoteSQL
    {
        [FieldConverter(typeof(GuidConverter))]
        public Guid CurrencyPairId;
        [FieldConverter(typeof(DateTimeSQLConverter))]
        public DateTime Time;
        public decimal Open;
        public decimal High;
        public decimal Low;
        public decimal Close;
        public int Volume;
    }
}
