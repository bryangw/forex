﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;
namespace ForexImporter
{
    [DelimitedRecord(",")]
    [IgnoreFirst(1)]
    class Pip
    {
        public string Pair { get; set; }
        public decimal pip { get; set; }
        public decimal cSpread { get; set; }
        public decimal pSpread { get; set; }
    }
}
