﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Forex.Models;

namespace ForexImporter.Models
{
    public class ThreadStartParameters
    {
        public Guid PairId { get; set; }
        public Enums.TimeFrame TimeFrame { get; set; }
    }
}
