﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Forex;
using Forex.Models;
using EntityFramework.BulkInsert.Extensions;
using System.Threading;
using EF = System.Data.Entity.DbFunctions;
using MongoDB.Driver;

namespace ForexImporter
{

    class Program
    {
        public static Forex.Data.Context db { get; set; }
        static void Main(string[] args)
        {
            //Programs.GenerateTimeFrames.Run();
            //Programs.CalculateDayofWeekTrends.Run();
            //Programs.Test.Run();
            //Programs.Trend.Run();
            //Programs.CalculateTradeValues.Run();
            //Programs.ImportMetaTraderTimeFrames.Run();
            Programs.Convert.Run();
        }
  

       
    }
}
