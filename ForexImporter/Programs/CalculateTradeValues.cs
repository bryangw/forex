﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForexImporter.Programs
{
    public static class CalculateTradeValues
    {
        public static void Run()
        {
            var db = new Forex.Data.Context();
            var Trades = Helpers.SQL.BulkRead<Forex.Models.Trade>("Select * From Trades");
            var Pairs = db.CurrencyPairs.ToList();
            decimal TotalPips = 0;
            Trades.ForEach(t =>
            {
                var Pair = Pairs.First(p=>p.Id==t.CurrencyPairId);
                if (t.Position == Forex.Models.Enums.Position.Long)
                    TotalPips += (t.ClosePrice - t.OpenPrice) / Pair.pip;
                else
                    TotalPips += (t.OpenPrice - t.ClosePrice) / Pair.pip;
            });
        }
    }
}
