﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Forex;
using Forex.Models;
using EntityFramework.BulkInsert.Extensions;
using System.Threading;
using EF = System.Data.Entity.DbFunctions;
using MongoDB.Driver;

namespace ForexImporter.Programs
{
    public static class GenerateTimeFrames
    {
        public static void Run()
        {
            var db = new Forex.Data.Context();
            db.CurrencyPairs.ToList().ForEach(Pair =>
            {
                Console.WriteLine(Pair.Pair);
                var Intervals = new List<Forex.Business.Interval>();
                var TimeFrames = new List<TimeFrameCollection>();
                foreach (Enums.TimeFrame e in Enum.GetValues(typeof(Enums.TimeFrame)))
                {
                    var Interval = new Forex.Business.Interval(e, Pair, new Forex.Connectors.NullConnector());
                    Intervals.Add(Interval);
                    var TimeFrame = new TimeFrameCollection() { PairId = Pair.Id, TimeFrame = e };
                    TimeFrames.Add(TimeFrame);
                    Interval.NewBar += interval =>
                    {
                        if (interval.Bars.Count == 1) { return; }
                        TimeFrame.Bars.Add(interval.Bars[interval.Bars.Count() - 1]);
                    };
                }
                var Quotes = Helpers.SQL.BulkRead<Forex.Models.Quote>("Select * from quotes where CurrencyPairId = '" + Pair.Id.ToString() + "' and Time > '2013-09-12' order by [time]");
                Quotes.ForEach(q =>
                {
                    Intervals.ToList().ForEach(i =>
                    {
                        i.PushBar(q);
                    });
                });
                TimeFrames.ForEach(f=> Forex.Data.FileSystem.SaveTimeFrame(f));
            });
        }

    }
}
