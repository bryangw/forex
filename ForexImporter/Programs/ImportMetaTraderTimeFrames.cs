﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trade.Models;

namespace ForexImporter.Programs
{
    public static class ImportMetaTraderTimeFrames
    {
        public static MtApi5.MtApi5Client mt { get; set; }
        public static bool isFinished = false;
        public static void Run(){
     
            mt = new MtApi5.MtApi5Client();
            
            mt.ConnectionStateChanged += new EventHandler<MtApi5.Mt5ConnectionEventArgs>(ConnectionStateChange);
            mt.BeginConnect(8228);
            do
            {
                if (isFinished)
                    Environment.Exit(0);

                System.Threading.Thread.Sleep(1000);
            }
            while (true);
        }
        private static void ConnectionStateChange(object sender, MtApi5.Mt5ConnectionEventArgs e)
        {
            if (e.Status != MtApi5.Mt5ConnectionState.Connected)
                return;
            var Quotes = mt.GetQuotes();
            var QuoteList = new List<string>();
            Quotes.ToList().ForEach(q=> QuoteList.Add(q.Instrument));
            Forex.Data.FileSystem.WriteList("Pairs", QuoteList);
            foreach (var q in Quotes)
            {
                foreach (MtApi5.ENUM_TIMEFRAMES t in Enum.GetValues(typeof(MtApi5.ENUM_TIMEFRAMES)))
                {
                    var TimeFrame = t.ToString().Split('_')[1];
                    if (TimeFrame == "CURRENT" || TimeFrame == "MN1")
                    {
                        continue;
                    }
                    var Period = (Enums.Periods)Enum.Parse(typeof(Enums.Periods), TimeFrame);
                    MtApi5.MqlRates[] array;
                    mt.CopyRates(q.Instrument, t, 0, 100000, out array);
                    if (array == null)
                        continue;

                    var Rates = new List<Rate>();
                        

                    foreach(var r in array)
                    {
                        var rate = new Rate()
                        {
                            Open = r.open,
                            Close = r.close,
                            High = r.high,
                            Low = r.low,
                            Time = r.time
                        };
                        Rates.Add(rate);
                    }
                    Trade.Data.FileSystem.SaveRates(q.Instrument, Period, Rates);
                }
            }
            isFinished = true;
            
            
        }

    }
}
