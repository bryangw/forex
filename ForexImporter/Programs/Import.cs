﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Forex;
using Forex.Models;
using EntityFramework.BulkInsert.Extensions;
using EF = System.Data.Entity.DbFunctions;

namespace ForexImporter.Programs
{
    public static class Import
    {
        public static void Run()
        {
            //ImportPairs();
            ImportPair();
            //ClearMissingDataPoints();
        }
        public static void ClearMissingDataPoints()
        {
            var db = new Forex.Data.Context();
            var CurrrencyPairs = db.CurrencyPairs.ToList();
            foreach (var cp in CurrrencyPairs)
            {
                db = new Forex.Data.Context();
                db.Configuration.AutoDetectChangesEnabled = false;
                db.Configuration.LazyLoadingEnabled = false;
                db.Configuration.ProxyCreationEnabled = false;
                db.Configuration.UseDatabaseNullSemantics = false;
                db.Configuration.ValidateOnSaveEnabled = false;
                Console.WriteLine("Loading Data For " + cp.Pair);
                var Quotes = db.Quotes.AsNoTracking().Where(q => q.CurrencyPairId == cp.Id).OrderBy(q => q.Time).ToList();
                Console.WriteLine("Data Loaded");
                var Inserts = 0;
                for (var x = 0; x < Quotes.Count() - 1; x++)
                {
                    var CurrentQuote = Quotes[x];
                    var NextQuote = Quotes[x + 1];
                    var NextQuoteMinusAMinute = NextQuote.Time.AddMinutes(-1);
                    var CurrentQuotePlusADay = CurrentQuote.Time.AddDays(1);
                    if (!(DateTime.Compare(CurrentQuote.Time, NextQuoteMinusAMinute) == 0) && DateTime.Compare(CurrentQuote.Time, NextQuote.Time) != 0 && (CurrentQuotePlusADay.DayOfWeek != DayOfWeek.Saturday || CurrentQuotePlusADay.DayOfWeek != DayOfWeek.Sunday) && DateTime.Compare(CurrentQuote.Time, NextQuote.Time.AddMinutes(-30)) > 0)
                    {
                        System.TimeSpan TimeSpan = NextQuote.Time - CurrentQuote.Time;
                        var Minutes = TimeSpan.Minutes;
                        var OpenPlus = (CurrentQuote.Open - NextQuote.Open) / Minutes;
                        var LowPlus = (CurrentQuote.Low - NextQuote.Low) / Minutes;
                        var HighPlus = (CurrentQuote.High - NextQuote.High) / Minutes;
                        var ClosePlus = (CurrentQuote.Close - NextQuote.Close) / Minutes;
                        var VolumePlus = (CurrentQuote.Volume - NextQuote.Volume) / Minutes;

                        for (var y = 1; y < Minutes; y++)
                        {
                            var NewQuote = new Forex.Models.Quote()
                            {
                                CurrencyPairId = CurrentQuote.CurrencyPairId,
                                Open = CurrentQuote.Open + (OpenPlus * y),
                                Low = CurrentQuote.Low + (LowPlus * y),
                                High = CurrentQuote.High + (HighPlus * y),
                                Close = CurrentQuote.Close + (ClosePlus * y),
                                Volume = CurrentQuote.Volume + (VolumePlus * y),
                                Time = CurrentQuote.Time.AddMinutes(y),
                                isInsert = true
                            };
                            db.Quotes.Add(NewQuote);
                        }
                        Console.WriteLine("Time difference: " + cp.Pair + " " + CurrentQuote.Time.ToShortDateString() + " " + CurrentQuote.Time.ToShortTimeString() + " to " + NextQuote.Time.AddMinutes(1).ToShortTimeString() + " " + NextQuote.Time.AddMinutes(1).ToShortTimeString());
                        db.SaveChanges();
                        Inserts++;
                        if (Inserts == 1000)
                        {
                            Console.WriteLine("Clearing Context");
                            db = new Forex.Data.Context();
                            db.Configuration.AutoDetectChangesEnabled = false;
                            db.Configuration.LazyLoadingEnabled = false;
                            db.Configuration.ProxyCreationEnabled = false;
                            db.Configuration.UseDatabaseNullSemantics = false;
                            db.Configuration.ValidateOnSaveEnabled = false;
                            Inserts = 0;
                        }
                    }
                }
                Console.WriteLine();
            }
        }
        public static void ImportPairs()
        {
            var engine = new FileHelpers.FileHelperEngine(typeof(Pip));
            Pip[] Pips = engine.ReadFile("d:\\forexdata\\pips.csv") as Pip[];
            var db = new Forex.Data.Context();
            db.Configuration.AutoDetectChangesEnabled = false;
            db.Configuration.ValidateOnSaveEnabled = false;
            foreach (var p in Pips)
            {
                var dbPair = new Forex.Models.CurrencyPair()
                {
                    Pair = p.Pair,
                    pip = p.pip,
                    pSpread = p.pSpread,
                    cSpread = p.cSpread
                };
                db.CurrencyPairs.Add(dbPair);

            }
            db.SaveChanges();
        }
        public static void ImportPair()
        {
            var db = new Forex.Data.Context();
            var Pairs = db.CurrencyPairs.ToList();

            var x = 0;
            FileHelpers.FileHelperEngine<QuoteSQL> qw;
            System.IO.StreamWriter sw;
            foreach (var Pair in Pairs)
            {
                var QuotesList = new List<Forex.Models.Quote>();
                //if (db.Quotes.Any(q => q.CurrencyPairId == Pair.Id)) { continue; }
                var engine = new FileHelpers.FileHelperAsyncEngine<Quote>();
                //Quote[] Quotes = engine.ReadFile() as Quote[];
                var TextReader = new System.IO.StreamReader("d:\\forexdata\\csv\\" + Pair.Pair + ".csv");
                engine.BeginReadStream(TextReader);

                var count = 0;

                do
                {
                    var q = engine.ReadNext();
                    x++;
                    count++;
                    var DateArr = q.Date.Split('.');
                    var TimeArr = q.Time.Split(':');
                    var dbQuote = new Forex.Models.Quote
                    {
                        Time = new DateTime(int.Parse(DateArr[0]), int.Parse(DateArr[1]), int.Parse(DateArr[2]), int.Parse(TimeArr[0]), int.Parse(TimeArr[1]), 0),
                        Open = q.Open,
                        Low = q.Low,
                        High = q.High,
                        Close = q.Close,
                        Volume = q.Volume,
                        CurrencyPairId = Pair.Id
                    };
                    QuotesList.Add(dbQuote);
                    if (x == 1000)
                    {
                        Console.Clear();
                        Console.WriteLine(count);
                        x = 0;

                    }
                } while (!TextReader.EndOfStream);
                TextReader.Close();
                db.BulkInsert(QuotesList);
                /*
                qw = new FileHelpers.FileHelperEngine<QuoteSQL>();
                sw = new System.IO.StreamWriter("d:\\forexdata\\SQL\\" + Pair.Pair + "_SQL.csv");
                sw.WriteLine("CurrencyPairId,Time,Open,Low,High,Close,Volume");
                qw.WriteStream(sw, QuotesList);
                sw.Close();
                QuotesList = new List<QuoteSQL>();
                 * */
            }

        }
    }
}
