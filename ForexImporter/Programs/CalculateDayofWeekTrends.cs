﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForexImporter.Programs
{
    public static class CalculateDayofWeekTrends
    {
        public static void Run()
        {
            var db = new Forex.Data.Context();
            var date = new DateTime(2013, 9, 12);
            var start = new DateTime(2009, 9, 12);
            db.CurrencyPairs.ToList().ForEach(p =>
            {
                Console.WriteLine(p.Pair);
                var Up = new int[7];
                var Down = new int[7];
                var Count = new int[7];
                var Measured = new int[7];
                var Mean = new double[7];
                var TimeFrame = Forex.Data.FileSystem.GetTimeFrame(p.Id, Forex.Models.Enums.TimeFrame.D);

                TimeFrame.Bars = TimeFrame.Bars.Where(t=>t.Time < date).OrderBy(t => t.Time).ToList();
                for (var x = 0; x < TimeFrame.Bars.Count; x++)
                {
                    var Today = TimeFrame.Bars[x];
                    Count[(int)Today.Time.DayOfWeek]++;

                    Mean[(int)Today.Time.DayOfWeek] += Math.Abs((double)Today.Close - (double)Today.Open);
                    if (Today.Close == Today.Open)//+ (decimal)2.5 * p.pip >= Yesterday.Close && Yesterday.Close <= Today.Close - (decimal)2.5 * p.pip)
                        continue;
                    Measured[(int)Today.Time.DayOfWeek]++;
                    if (Today.Open < Today.Close)
                        Up[(int)Today.Time.DayOfWeek]++;
                    else
                        Down[(int)Today.Time.DayOfWeek]++;
                    
                }
                var ub = Count.GetUpperBound(0);
                for (var x = 0; x <= ub; x++)
                {
                    var DayofWeek = new Forex.Models.Indicators.DayofWeek()
                    {
                        Id = Guid.NewGuid(),
                        CurrencyPairId = p.Id,
                        DayOfWeek = (Forex.Models.Enums.DaysOfWeek)x
                    };
                    DayofWeek.Up = ((decimal)Up[x] / Count[x]) * 100;
                    DayofWeek.Down = ((decimal)Down[x] / Count[x]) * 100;
                    DayofWeek.Mean = (decimal)Mean[x] / Count[x];
                    double dUp = 0;
                    double dDown = 0;
                    decimal Direction = 0;
                    TimeFrame.Bars.Where(t=> (int)t.Time.DayOfWeek==x).ToList().ForEach(b=>{
                        if (Count[x] == 0)
                            return;
                        Direction = Math.Abs(b.Close - b.Open) - DayofWeek.Mean;
                        DayofWeek.StandardDeviation += (decimal)Math.Pow((double)Direction, 2);
                    });
                    DayofWeek.StandardDeviation = (decimal)Math.Sqrt((double)DayofWeek.StandardDeviation/Measured[x]);
                    DayofWeek.SafeBet = Math.Abs(DayofWeek.Up-DayofWeek.Down) - DayofWeek.StandardDeviation;
                    if (DayofWeek.Up + DayofWeek.Down > 95)
                    {
                        db.Indicators_DayofWeek.Add(DayofWeek);
                    }
                }
                db.SaveChanges();
            });
        }
    }
}
