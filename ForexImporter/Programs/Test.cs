﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForexImporter.Programs
{
    public static class Test
    {
        public static double Gains { get; set; }
        public static double Losses { get; set; }
        public static int Pips { get; set; }
        public static DateTime TradeOpenUntil { get; set; }
        public static Forex.Models.TimeFrameCollection M15 { get; set; }
        public static Forex.Models.TimeFrameCollection D { get; set; }
        public static void Run()
        {
            var db = new Forex.Data.Context();
            var DaysofWeek = db.Indicators_DayofWeek.ToList();
            var date = new DateTime(2013, 9, 12);
            db.CurrencyPairs.ToList().ForEach(p =>
            {
                var TimeFrame = Forex.Data.FileSystem.GetTimeFrame(p.Id, Forex.Models.Enums.TimeFrame.M1);
                var Connector = new Forex.Connectors.TestConnector(p.Pair, Forex.Models.Enums.TimeFrame.M1);
                var Interval = new Forex.Business.Interval(Forex.Models.Enums.TimeFrame.M15, p, Connector);

                Interval.Indicators.Add(new Forex.Indicators.DayOfWeek(Interval));
                Interval.Indicators.Add(new Forex.Indicators.Trend(Interval, 5));
                Interval.Indicators.Add(new Forex.Indicators.Trend(Interval, 10));
                Interval.Indicators.Add(new Forex.Indicators.Trend(Interval, 15));
                Interval.Indicators.Add(new Forex.Indicators.Trend(Interval, 20));

                var Set = TimeFrame.Bars.OrderBy(b => b.Time).ToList();
                foreach (var b in Set)
                {
                    Interval.PushBar(b);
                }
            });
        }
    }
}
